nuget restore src/WebApplication2.sln

SonarScanner.MSBuild.exe begin /k:"TestProject" /d:sonar.analysis.mode=publish /d:sonar.gitlab.commit_sha=%CI_COMMIT_SHA% /d:sonar.gitlab.project_id=%CI_PROJECT_ID% /d:sonar.gitlab.ref_name=%CI_COMMIT_REF_NAME%
MSBuild.exe src/WebApplication2.sln /t:Rebuild
SonarScanner.MSBuild.exe end
